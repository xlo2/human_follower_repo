# README #

Project name:

	* human_follower
	
Members:

	* Richard Raasuke
	* Ilmar Vodi
	* Luka Janjgava, e-mail: luka.janjgavaa@gmail.com

Overview:

	Our project's topic is human-following robot. The idea is to create a working robot that is 
	capable of following one human who moves around and can change their walking speed as they wish, 
	so the robot will act accordingly. We assume that there will be only one human in the room and 
	we know the colour, which person is wearing. We plan to use GoPiGo as our platform, so the code
	will be written in python. Our idea is to detect the person with PIR sensor and camera image processing.
	Ultrasonic sensor is used to measure the distance. We have scheduled a timetable for ourselves 
	where we have divided all the subtasks with certain deadlines which we try to keep a hold of. 
	
Schedule for pair A:

	12.11 – Implement PIR sensor
	19.11 – Implement ultrasonic sensor
	26.11 – Write the following logic
	3.12 – Implement the hardware solution and combine the software
	10.12 – Make a poster
	17.12 – Check everything and fix all remaining problems

Schedule for pair B:

	17.11 - Get live video feed
	24.11 - start image processing
	26.11 - Make demo video
	01.12 - Finish image processing
	08.12 - Implement movement to detected object
	10.12 - Make a poster
	17.12 - final touch, live demo ready

Component list:

	* GoPiGo 2
	* RaspberryPi 3
	* Ultrasonic sensor
	* Arduino Nano
	* Breadbord
	* Camera
	* SD card
	* battery and alarm

Challenges and solutions

	Our main challenge that we have to face right now is to implement motion detection. The 
	problem,  which  occurs as we try to apply our current skills to it is the lack of 
	knowledge on our part. Also it takes some time to learn how it works and 
	how to implement it to our project, but we are working on it and we hope to see the 
	results soon. Our second big challenge is time in general. As our group has decreased from 6 members 
	to 3 members, we have to rely on less people than planned. That also means we had to change the way we 
	work. We do not have two pairs anyomre, instead we are all working on same task at once. Other than 
	that, we also have to change our time schedule so we can be ready in time.
	