#!/usr/bin/env python3
# coding=utf-8
import serial
import gopigo as go
import time
import cv2
import json
import os
import time
import numpy as np

print("Battery voltage: " + str(go.volt()))

ser = serial.Serial('/dev/ttyUSB0', 9600)

try:
    leftSpeed = 60
    rightSpeed = (leftSpeed + 2)
    go.set_right_speed(rightSpeed)
    go.set_left_speed(leftSpeed)
    dist = -1

    # Make sure arduino is ready to send the data.
    print("Syncing serial...0%\r", end='')
    while ser.in_waiting == 0:
        ser.write("R".encode())
    print("Syncing serial...50%\r", end='')
    while ser.in_waiting > 0:
        ser.readline()
    print("Syncing serial...100%")


    while True:

        # Read the serial input to string
        ser.write("R".encode()) # Send something to the Arduino to indicate we're ready to get some data.
        serial_line = ser.readline().strip() # Read the sent data from serial.

        try:

            # Decode the received JSON data
            data = json.loads(serial_line.decode())
            # Extract the sensor values
            dist = data['us1']
        except Exception as e:  # Something went wrong extracting the JSON.
            dist = -1           # Handle the situation.
            print(e)
            pass


        if dist != -1: # If a JSON was correctly extracted, continue.
            # Print received to the console
            print("DIST: ", dist)

            # Line following logic goes here
            if dist > 700:
                go.fwd()
            else:
                go.stop()

except KeyboardInterrupt:
    print("Serial closed, program finished")

finally:
    ser.close()
    running = False # Stop other threads.
go.stop()


