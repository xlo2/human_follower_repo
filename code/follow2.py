#!/usr/bin/env python3
# coding=utf-8

#Importimised
import serial
import gopigo as go
import time
import cv2
import json
import os
import time
import numpy as np

print("Battery voltage: " + str(go.volt()))

#GoPiGo kiirused
go.set_right_speed(152)
go.set_left_speed(150)

#Defineerime muutujad
dist = -1
idle_time = 0
status = "motion"
back = None

#Määrame andmete vahetuse sageduse (bit per second)
ser = serial.Serial('/dev/ttyUSB0', 9600)

video = cv2.VideoCapture()
ret, frame = video.read()
r = [len(frame) - 220, len(frame) - 170, 0, len(frame[0])]
tracker = cv2.TrackerMIL_create()

try:

    # Ootame, et Arduino oleks valmis andmete vahetuseks.
    print("Syncing serial...0%\r", end='')
    while ser.in_waiting == 0:
        ser.write("R".encode())
    print("Syncing serial...50%\r", end='')
    while ser.in_waiting > 0:
        ser.readline()
    print("Syncing serial...100%")


    while True:
        # Loeme serial sisendi
        ser.write("R".encode()) # Saadame Arduinole midagi, et näidata, et me oleme valmis info saamiseks
        serial_line = ser.readline().strip() # Loeme serialist saadud  andmed
        try:
            # Dekodeerime saadud andmed
            data = json.loads(serial_line.decode())
            # Saame kätte kaugused
            dist = data['us1']
        #Kui on viga
        except Exception as e:
            dist = -1
            print(e)

        #Kui saime kaugused kätte
        if dist != -1:
            # Väljastame kaugused
            print("DIST: ", dist)

        #Kaadrite lugemine
        ret, frame = video.read()
        while not ret:
            ret, frame = video.read()

        #Teeme kaamera pidi kitsamaks ja muudame pildi grayks# Rakendame Gaussian bluri
        gray = cv2.GaussianBlur(cv2.cvtColor(frame[r[0]:r[1], r[2]:r[3]], cv2.COLOR_BGR2GRAY), (21, 21), 0)

        #Anname taustale väärtuseks gray, et kahte kaadrit võrrelda
        if back is None:
            back = gray

        #Leiame kahe kaadri erinevused
        if status == "motion":
            frame_difference = cv2.absdiff(back, gray)

            #Toome välja keha piirjooned
            _, cnts, hierarchy = cv2.findContours(cv2.dilate(cv2.threshold(frame_difference, 25, 255, cv2.THRESH_BINARY)[1], None, iterations = 2), cv2.RETR-EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

            #Leiame suurima tuvastatud objekti
            if len(cnts) != 0:
                largest = 0

                for cnt in range(len(cnts)):
                    if cnt != 0 and int(cv2.contourArea(cnts[cnt])) > int(cv2.contourArea(cnts[largest])):
                        largest = cnt

                #Seome tuvastatud objekti trackeriga
                if cv2.contourArea(cnts[largest]) > 500:
                    (x, y, w, h) = cv2.boundingRect(cnts[0])
                    (x,y,w,h) = (int(x),int(y),int(w),int(h))
                    bbox = (x,y,w,h)
                    ret = tracker(frame, bbox)
                    status = "tracking"

        #Jälgime keha liikumist
        elif status == "tracking":
            ret, bbox = tracker.update(frame)

            #Teeme kujutise nähtavaks
            if ret:
                p1 = (int(bbox[0]), int(bbox[1]))
                p2 = (int(bbox[0]) + int(bbox[2]), int(bbox[1]) + int(bbox[3]))
                cv2.rectangle(frame, p1, p2, (0, 0, 255), 10)

                #Liilkumisloogika
                if dist > 700:
                    if x + w // 2 > 320:
                        go.set_left_speed(150)
                        go.set_right_speed(50)
                        go.fwd()
                    elif x + w // 2 < 320:
                        go.set_left_speed(50)
                        go.set_right_speed(150)
                        go.fwd()
                    else:
                        go.set_left_speed(150)
                        go.set_right_speed(152)
                        go.fwd()
                else:
                    if x + w // 2 > 320:
                        go.set_speed(150)
                        go.right_rot()
                    elif x + w // 2 < 320:
                        go.set_speed(150)
                        go.left_rot()
                    else:
                        go.set_speed(0)

        #Näitame pilti
        cv2.imshow("Camera", frame)

        #Kordame protsessi iga 30 kaardri tagant ei objekti mitte kaamera pildist kaotada
        if idle_time >= 30:
            status = "motion"
            idle_time = 0

            back = None
            tracker = None
            ret = None

            tracker = cv2.TrackerMIL_create()

        idle_time += 1

#Programmi sulgemise korral
except KeyboardInterrupt:
    print("Serial closed, program finished")

finally:
    ser.close()
go.stop()



#proov123